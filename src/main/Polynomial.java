/** Integer-only polynomials. */
public class Polynomial {
	private int[] x;
    /** Create new instance with given coefficients.
     *
     * x^2 + 3x - 7 would be created by new Polynomial(-7, 3, 1)
     * x^5 + 42 would be created by new Polynomial(42, 0, 0, 0, 0, 1)
     *
     * @param coef Coefficients, ordered from lowest degree (power).
     */
    public Polynomial(int... coef) {
    	x = coef.clone();
        /*System.out.printf("Creating polynomial");
        for (int i = 0; i < coef.length; i++) {
            if (i > 0) {
                System.out.print(" +");
            }
            System.out.printf(" %d * x^%d", coef[i], i);
        }
        System.out.println(" ...");*/
    }
    
    public Polynomial(Polynomial a) {
        x = a.x.clone();
    }

    /** Get coefficient value for given degree (power). */
    public int getCoefficient(int deg) {
    	if (deg >= x.length) {
            return 0;
        } else {
            return x[deg];
        }
    }

    /** Get degree (max used power) of the polynomial. */
    public int getDegree() {
    	for (int i = x.length - 1; i >= 0; i--) {
            if (x[i] != 0) {
                return i;
            }
        }
        return 0;
    }

    /** Format into human-readable form with given variable. */
    public String toPrettyString(String variable) {
    	StringBuilder a = new StringBuilder();
        int degree = getDegree();
        int coef = getCoefficient(degree);

        if (degree == 0) {
            a.append(coef);
        } else {
            if (coef == -1) a.append('-');
            else if (coef != 1) a.append(coef);

            a.append(variable);
            if (degree > 1) {
                a.append('^');
                a.append(degree);
            }
        }

        for (int i = degree - 1; i >= 0; i--) {
            coef = getCoefficient(i);

            if (coef == 0) continue;
            if (coef < 0) {
                a.append(" - ");
                coef = -coef;
            } else {
                a.append(" + ");
            }

            if (i == 0) {
                a.append(coef);
            } else { 
                if (coef != 1) a.append(coef);
                a.append(variable);
                if (i > 1) {
                    a.append('^');
                    a.append(i);
                }
            }
        }
        return a.toString();
    }

    /** Debugging output, dump only coefficients. */
    @Override
    public String toString() {
    	StringBuilder a = new StringBuilder("Polynomial[");
        for (int i = 0; i < x.length; i++) {
            if (i > 0) a.append(',');
            a.append(x[i]);
        }
        a.append(']');
        return a.toString();
    }
    
    private static int maxDegree(Polynomial... polynomials) {
        int maxDegree = polynomials[0].getDegree();
        int a;
        for (int i = 1; i < polynomials.length; i++) {
            a = polynomials[i].getDegree();
            if (a > maxDegree) maxDegree = a;
        }
        return maxDegree;
    }
    
    
    /** Adds together given polynomials, returning new one. */
    public static Polynomial sum(Polynomial... polynomials) {
    	if (polynomials.length < 1) return new Polynomial(0);
        if (polynomials.length == 1) return new Polynomial(polynomials[0]);

        int maxDegree = maxDegree(polynomials);

        int[] result = new int[maxDegree + 1];

        for (int i = 0; i <= maxDegree; i++) {
            result[i] = 0;
            for (Polynomial p : polynomials) {
                result[i] += p.getCoefficient(i);
            }
        }

        return new Polynomial(result);
    }

    /** Multiplies together given polynomials, returning new one. */
    public static Polynomial product(Polynomial... polynomials) {
        return new Polynomial(0);
    }

    /** Get the result of division of two polynomials, ignoring remaineder. */
    public static Polynomial div(Polynomial dividend, Polynomial divisor) {
        return new Polynomial(0);
    }

    /** Get the remainder of division of two polynomials. */
    public static Polynomial mod(Polynomial dividend, Polynomial divisor) {
        return new Polynomial(0);
    }
}
