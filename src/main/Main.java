public class Main {
    public static void main(String[] args) {
        if (args.length < 4) {
            printHelp();
            return;
        }

        String op = args[0];

        int delimPosition = 0;
        for (int i = 1; i < args.length; i++) {
            if (args[i].equals("--")) {
                delimPosition = i;
                break;
            }
        }
        if (delimPosition == 0) {
            printHelp();
            return;
        }

        int[] coefFirst = parseCoefficients(args, 1, delimPosition);
        int[] coefSecond = parseCoefficients(args, delimPosition + 1, args.length);

        if ((coefFirst.length == 0) || (coefSecond.length == 0)) {
            printHelp();
            return;
        }

        Polynomial first = new Polynomial(coefFirst);
        Polynomial second = new Polynomial(coefSecond);

        Polynomial result;
        String opSymbol;
        if (op.equals("plus")) {
            result = Polynomial.sum(first, second);
            opSymbol = "+";
        } else {
            printHelp();
            return;
        }

        System.out.printf("(%s) %s (%s) = (%s)\n",
                first.toPrettyString("x"),
                opSymbol,
                second.toPrettyString("x"),
                result.toPrettyString("x"));
    }

    private static void printHelp() {
        System.out.println("ERROR");
        
    }

    private static int[] parseCoefficients(String[] args, int firstIndexIncl, int lastIndexExcl) {
        int[] result = new int[lastIndexExcl - firstIndexIncl];
        int resIndex = result.length - 1;
        for (int i = firstIndexIncl; i < lastIndexExcl; i++) {
            result[resIndex] = Integer.parseInt(args[i]);
            resIndex--;
        }
        return result;
    }

}